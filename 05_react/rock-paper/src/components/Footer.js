import { useEffect } from "react";

export default function Footer({ name, age }) {
    useEffect(() => {
        console.log("Footer shown");
        return () => console.log("Footer hidden");
    }, []);

    return (
        <footer>
            <h1>
                Footer 1 {name} {age}
            </h1>
        </footer>
    );
}
